---
# Custom Footer Content
footer:
    right: true
    logo: logos/markdown.png

# Pandoc Integration
bold: true
quote: true
toc: true
---

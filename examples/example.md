---
# Personal Settings
title: Pandoc Template for Beamer
subtitle: An Elementary Theme
author: Romain Wallon
institute: Pandoc Toolkit

# Event Information
event: Release 0.1.0
date: September 2019

# Logos
logo:
    - logos/markdown.png
    - logos/latex.png
logo-height: 1.4cm
---

# Introduction

## From Beamer to Pandoc

### Beamer

**Beamer** is a `LaTeX` document class for creating presentation slides, with a
wide range of templates and a set of features for making slideshow effects.

Its name is taken from the German word *Beamer* as a pseudo-anglicism for
*video projector*.

### Pandoc

**Pandoc** is a Haskell library for converting from one markup format to
another, and a command-line tool that uses this library.

### Why a Pandoc Template for Beamer?

To use Pandoc to create a Beamer presentation, you may run the following
command:

```bash
$ pandoc -t beamer \
         -o output.pdf input.md
```

However, this often requires to write `LaTeX` code if you want to customize
your presentation.

We propose instead to design a highly-customizable Pandoc template.

# Writing your Presentation

## Elements

### Typography

Because Markdown is limited in terms of typography, you cannot combine bold and
alert fonts:

```markdown
The template provides sensible defaults to
*emphasize* text, and to show **strong**
results.
```

becomes

The template provides sensible defaults to *emphasize* text, and to show
**strong** results.

### Lists

There is a support for items:

+ Milk
+ Eggs
+ Potatoes

Enumerations are also supported:

1. First,
2. Second and
3. Last.

### Animations

Unfortunately, Markdown does not support Beamer animations...

\pause

But you can still write \LaTeX!

\pause

Simply put a `\pause`, and it will work perfectly!

### Figures

![This presentation is written in Mardown.](logos/markdown.png)

### Tables

Tables are supported through Pandoc's Markdown.

The largest cities in the world (source: Wikipedia) are:

| City         | Population  |
|--------------|-------------|
| Mexico City  | 20,116,842  |
| Shanghai     | 19,210,000  |
| Peking       | 15,796,450  |
| Istanbul     | 14,160,467  |

### Punchlines

*Punchlines* are implemented through the `quote` environment, unless you
specified that you did not want them:

```markdown
> Isn't it great?
```

They are used for *plot-twists* in your presentations, or for *take-away
messages*.

\pause

> Isn't it great?

# Conclusion

### Conclusion

Get the source of the template and all the examples on
[GitLab](https://gitlab.com/pandoc-toolkit-by-rwallon/beamer-templates/elementary).

> This template is licensed under a Creative Commons Attribution 4.0
> International License.

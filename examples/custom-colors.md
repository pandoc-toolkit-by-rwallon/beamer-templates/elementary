---
# Color Definitions
color:
    - name: myblue
      type: RGB
      value: 107,174,214
    - name: mygreen
      type: RGB
      value: 102,255,204
    - name: myblack
      type: RGB
      value: 24,24,24

# Color Settings
title-color: myblue
subtitle-color: mygreen
institute-color: gray
background-color: myblack
main-color: white
alert-color: mygreen
box-color: darkgray

# Custom Footer Content
footer:
    text: This is a dark presentation!
---

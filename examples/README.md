# Use Cases of the *Elementary* Template

This directory contains examples illustrating different use cases of the
*Elementary* Pandoc template for Beamer.

All the examples are based on the same Markdown source, available
[here](./example.md).
The different outputs are only obtained by using different metadata files.

Observe that, in these examples, slide titles are level-3 titles in the
Markdown source.
Hence, `SLIDE_LEVEL` has to be set to `3`.

For each of the examples, you may either read its metadata file (to see how we
configured it) or download the final PDF.

## Default Configuration

In this example, there are no additional settings.
The presentation is produced based on the default configuration.

*Metadata file available [here](default.md).*

*PDF available [here](/../builds/artifacts/master/file/examples/default.pdf?job=make-examples).*

## Custom Style

This example deactivates the custom handling of bold font and quote
environments.

Also, an image is added in the footer of the slides, and its location is
changed so as to put it on the right.

Finally, a table of contents is displayed on the second slide.

*Metadata file available [here](custom-style.md).*

*PDF available [here](/../builds/artifacts/master/file/examples/custom-style.pdf?job=make-examples).*

## Custom Colors

This example uses a custom set of colors for the presentation, inspired by
[Karl Broman's presentation](https://github.com/kbroman/Talk_OpenAccess).

Also, a custom footer is added at the default location.

*Metadata file available [here](custom-colors.md).*

*PDF available [here](/../builds/artifacts/master/file/examples/custom-colors.pdf?job=make-examples).*

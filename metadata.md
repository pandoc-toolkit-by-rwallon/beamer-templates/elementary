---
# Base Preamble Settings
handout:
language:
package:
    -
header-includes:
    -
include-before:
    -

# Color Definitions
color:
    - name:
      type:
      value:

# Color Settings
title-color:
subtitle-color:
author-color:
institute-color:
background-color:
frametitle-color:
frametitle-background:
main-color:
alert-color:
box-color:

# Font Settings
sans-font:
mono-font:

# Custom Footer Content
footer:
    right:
    text:
    logo:

# Pandoc Integration
bold:
quote:
toc:
links-as-notes:

# Personal Settings
title:
subtitle:
author:
    -
institute:
    -

# Event Information
event:
date:

# Logos
logo:
    -
logo-height:

# Final Highlights
highlights-title:
highlights-width:
highlights-rows:
    - frames:
        -

# Bibliography
biblio-style:
natbiboptions:
    -
bibliography:
    -
---
